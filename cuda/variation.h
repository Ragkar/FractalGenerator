#pragma once
#include "struct_utils.h"

typedef point (*Variation)(double, double);

__device__
struct point v0(double x, double y);

__device__
struct point v1(double x, double y);

__device__
struct point v2(double x, double y);

__device__
struct point v3(double x, double y);

__device__
struct point v4(double x, double y);

__device__
struct point v5(double x, double y);

__device__
struct point v6(double x, double y);

__device__
struct point v7(double x, double y);

__device__
struct point v8(double x, double y);

__device__
struct point v9(double x, double y);

__device__
struct point v10(double x, double y);

__device__
struct point v11(double x, double y);

__device__
struct point v12(double x, double y);
/*
__device__
struct point v13(double x, double y);
*/
