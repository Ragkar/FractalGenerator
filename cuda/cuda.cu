#include <cmath>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

#include <curand.h>
#include <curand_kernel.h>
#include <png++/png.hpp>

#include "struct_utils.h"
#include "variation.h"

#define HEIGHT (1080)
#define WIDTH (1920)
#define BOUNDARY (HEIGHT * WIDTH)
#define ITER (1e9)
#define N_THREAD (1024)
#define N_BLOCK (5)

#define cudaCheckError                                                                           \
        cudaError_t e=cudaGetLastError();                                                        \
        if(e!=cudaSuccess) {                                                                     \
            printf("Cuda failure %s:%d: '%s'\n",__FILE__,__LINE__,cudaGetErrorString(e));        \
            exit(EXIT_FAILURE);                                                                  \
        }

/*-- Variation --------------------------------------------------------------*/
__device__
Variation d_variations[] =
{
  v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12
};

/*-- Function device --------------------------------------------------------*/

__global__
void kernel_run(int sz, int *vars, struct coefficient *coefs,
                struct color *colors, struct color *img)
{
  const int k = threadIdx.x + blockDim.x * blockIdx.x;

  curandState_t state;
  curand_init(clock64() * k, 0, k, &state);

  double rnd1 = curand(&state) % RAND_MAX;
  double rnd2 = curand(&state) % RAND_MAX;
  point p = { rnd1 / RAND_MAX, rnd2 / RAND_MAX };

  for (unsigned int i = 0; i < (ITER / (N_THREAD * N_BLOCK)); ++i)
  {
    unsigned int j = curand(&state);
    j %= sz;

    struct coefficient *cf = &coefs[j];
    Variation v = d_variations[vars[j]];
    p = v(cf->x1 * p.x + cf->y1 * p.y + cf->c1,
          cf->x2 * p.x + cf->y2 * p.y + cf->c2);
    const int x = (p.x + 1) * WIDTH / 2;
    const int y = (p.y + 1) * HEIGHT / 2;

    const size_t idx = y * WIDTH + x;
    if (idx < BOUNDARY)
    {
      struct color *cl = &colors[j];
      atomicAdd(&img[idx].r, cl->r);
      atomicAdd(&img[idx].g, cl->g);
      atomicAdd(&img[idx].b, cl->b);
      atomicAdd(&img[idx].a, 1);
    }
    else
      --i;
  }
}
/*-- Main -------------------------------------------------------------------*/

int main()
{
  /*
   * Index of the variations, here we use 3 variations: 1, 9 and 3
   */  
  int h_vars[] =
  {
    1, 9, 3
  };
  int *d_vars;
  cudaMalloc(&d_vars, sizeof(h_vars));
  cudaMemcpy(d_vars, h_vars, sizeof(h_vars), cudaMemcpyHostToDevice);

  /*
   * Coefficients can be modified. There is as many array of 6 coeeficient as
   * there is variation. 3 variations => 3 arrays of 6 coefficients.
   */
  struct coefficient h_coefs[] =
  {
    {  0.98396,   0.298328,  0.359416,  -0.583541, -0.85059,  -0.378754},
    { -0.900388,  0.293063,  0.397598,   0.0225629, 0.465126, -0.277212},
    { -0.329863, -0.0855261, -0.369381, -0.858379,  0.977861,  0.547595}
  };
  struct coefficient *d_coefs = NULL;
  cudaMalloc(&d_coefs, sizeof(h_coefs));
  cudaMemcpy(d_coefs, &h_coefs, sizeof(h_coefs), cudaMemcpyHostToDevice);

  /*
   *  One color per variation, color are represented with rgb
   */
  struct color h_colors[] =
  {
    { 226.0 / 255.0, 162.0 / 255.0, 29.0 / 255.0, 1.0 },
    { 222.0 / 255.0, 95.0 / 255.0, 76.0 / 255.0, 1.0 },
    { 198.0 / 255.0, 47.0 / 255.0, 15.0 / 255.0, 1.0 }
  };
  struct color *d_colors = NULL;
  cudaMalloc(&d_colors, sizeof(h_colors));
  cudaMemcpy(d_colors, &h_colors, sizeof(h_colors), cudaMemcpyHostToDevice);

  struct color *d_img = NULL;
  const size_t max = (size_t)HEIGHT * (size_t)WIDTH * sizeof(struct color);
  cudaMalloc(&d_img, max);
  cudaMemset(d_img, 0, max);

  { cudaCheckError; }
  kernel_run<<<N_BLOCK,N_THREAD>>> (3, d_vars, d_coefs, d_colors, d_img);
  { cudaCheckError; }

  struct color *h_img = (struct color*)malloc(max);
  cudaMemcpy(h_img, d_img, max, cudaMemcpyDeviceToHost);

  png::image<png::rgb_pixel> rgb_img(WIDTH, HEIGHT);
  for (unsigned int x = 0; x < WIDTH; ++x)
  {
    for (unsigned int y = 0; y < HEIGHT; ++y)
    {
      int i = y * WIDTH + x;
      double s = (1 - (std::log1p(h_img[i].a) / h_img[i].a)) * 255.0;
      rgb_img[y][x] = png::rgb_pixel(h_img[i].r / h_img[i].a * s,
                                     h_img[i].g / h_img[i].a * s,
                                     h_img[i].b / h_img[i].a * s);
    }
  }
  rgb_img.write("fractal.png");

  { cudaCheckError; }
  return 1;
}
