#include <cmath>
#include "variation.h"

__device__
struct point v0(double x, double y)
{
  struct point p = { x, y };
  return p;
}

__device__
struct point v1(double x, double y)
{
  struct point p = { - std::sin(x), std::sin(y) };
  return p;
}

__device__
struct point v2(double x, double y)
{
  // TODO: check if sqrt is needed
  double r = std::sqrt(x * x + y * y);
  struct point p = { x / r, y / r};
  return p;
}

__device__
struct point v3(double x, double y)
{
  double r2 = x * x + y * y;
  struct point p = { x * std::sin(r2) - y * std::cos(r2),
              x * std::cos(r2) + y * std::sin(r2) };
  return p;
}

__device__
struct point v4(double x, double y)
{
  double r = std::sqrt(x * x + y * y);
  struct point p = { (x - y) * (x + y) / r, 2 * x * y / r };
  return p;
}

__device__
struct point v5(double x, double y)
{
  double theta = std::atan(x/y);
  double r = std::sqrt(x * x + y * y);
  struct point p = { theta / M_PI, r - 1 };
  return p;
}

__device__
struct point v6(double x, double y)
{
  double theta = std::atan(x/y);
  double r = std::sqrt(x * x + y * y);
  struct point p = { r * std::sin(theta + r) , r * std::cos(theta - r) };
  return p;
}

__device__
struct point v7(double x, double y)
{
  double theta = std::atan(x/y);
  double r = std::sqrt(x * x + y * y);
  struct point p = { r * std::sin(theta * r) , - r * std::cos(theta * r) };
  return p;
}

__device__
struct point v8(double x, double y)
{
  double theta = std::atan(x/y);
  double r = std::sqrt(x * x + y * y);
  struct point p = { (theta / M_PI) * std::sin(M_PI * r),
           (theta / M_PI) * std::cos(M_PI * r) };
  return p;
}

__device__
struct point v9(double x, double y)
{
  double theta = std::atan(x/y);
  double r = std::sqrt(x * x + y * y);
  struct point p = { (std::cos(theta) + std::sin(r)) / r,
           (std::sin(theta) - std::cos(r)) / r };
  return p;
}

__device__
struct point v10(double x, double y)
{
  double theta = std::atan(x/y);
  double r = std::sqrt(x * x + y * y);
  struct point p = { std::sin(theta) / r, r * std::cos(theta) };
  return p;
}

__device__
struct point v11(double x, double y)
{
  double theta = std::atan(x/y);
  double r = std::sqrt(x * x + y * y);
  struct point p = { (std::sin(theta) * std::cos(r)),
           (std::cos(theta) - std::sin(r)) };
  return p;
}

__device__
struct point v12(double x, double y)
{
  double r = std::sqrt(x * x + y * y);
  double theta = std::atan(x/y);
  double p0 = std::sin(theta + r);
  double p1 = std::cos(theta - r);
  p0 *= p0 * p0;
  p1 *= p1 * p1;
  struct point p = { r * (p0 + p1), r * (p0 - p1) };
  return p;
}
/*
__device__
struct point v13(double x, double y)
{
  double r = std::sqrt(std::sqrt(x * x + y * y));
  double theta = std::atan(x/y) / 2;
  double omega = (rand() % 2) * M_PI;
  struct point p = { r * std::cos(theta + omega), r * std::sin(theta + omega) };
  return p;
}*/
