#pragma once

struct point
{
  double x;
  double y;
};

struct color
{
  double r;
  double g;
  double b;
  double a;
};

struct coefficient
{
  double x1;
  double y1;
  double c1;
  double x2;
  double y2;
  double c2;
};
