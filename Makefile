CC      = g++
NVCC    = nvcc
CXXFLAGS = -std=c++17 -Wall -Werror -pedantic -I.
LDFLAGS = `libpng-config --cflags --ldflags`

OBJS = fractGen.o 	\
			 flame.o			\
			 point.o			\
			 color.o			\
			 flameFun.o		\
			 variation.o

SRC_CUDA = cuda/cuda.cu        \
          cuda/variation.cu

OBJS_CUDA = cuda.o            \
            variation.o

CUDA_OLD = cuda-old/fractGen.cu  \
					 cuda-old/flame.cu		 \
					 cuda-old/point.cu		 \
					 cuda-old/color.cu		 \
					 cuda-old/flameFun.cu  \
					 cuda-old/variation.cu

VPATH = cpu
BIN_CPU = fractGen
BIN_CUDA = fractGenCuda
BIN_OLD = fractGenOld

all: $(BIN_CPU)

$(BIN_CPU): $(OBJS)
	$(CC) $^ $(LDFLAGS) $(CXXFLAGS) -o $@

$(BIN_CUDA): $(SRC_CUDA)
	$(NVCC) $^ $(LDFLAGS) --std=c++11 -I. -dc -arch=sm_61 -O3
	$(NVCC) $(OBJS_CUDA) $(LDFLAGS) --std=c++11 -I. -arch=sm_61 -o $@

$(BIN_OLD): $(CUDA_OLD)
	$(NVCC) $(SRC_CUDA) $(LDFLAGS) --std=c++11 -I. -dc -arch=sm_61 -O3
	$(NVCC) $(OBJS) $(LDFLAGS) --std=c++11 -I. -arch=sm_61 -O3 -o $@

clean:
	$(RM) $(BIN_CPU) $(BIN_CUDA) $(BIN_OLD) $(OBJS) $(OBJS_CUDA) fractal.png

re: clean all

.PHONY: $(BIN_CPU) $(BIN_CUDA) $(BIN_OLD)
