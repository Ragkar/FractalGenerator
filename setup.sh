#!/bin/sh
#-- Download lib -------------------------------------------------------------#
lib="png++-0.2.9"
package=$lib.tar.gz


wget download.savannah.nongnu.org/releases/pngpp/$package
tar xvf $package
mv $lib png++
rm -r $package
