#pragma once

#include <functional>

#include "point.h"

typedef WPoint (*Variation) (double, double);

__device__ WPoint v0(double x, double y);
__device__ WPoint v1(double x, double y);
__device__ WPoint v2(double x, double y);
__device__ WPoint v3(double x, double y);
__device__ WPoint v4(double x, double y);
