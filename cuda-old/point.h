#pragma once

class WPoint
{
public:
  __device__ __host__ WPoint();
  __device__ __host__ WPoint(const WPoint& p);
  __device__ __host__ WPoint(double x, double y);
  __device__ __host__ WPoint(double x, double y, unsigned int c);

  __device__ __host__ void add(const WPoint& p);
  __device__ __host__ void mult(double d);

  double x;
  double y;
  unsigned char c;
};
