#include "flameFun.h"
#include "point.h"
#include "variation.h"
#include <iostream>

__device__ Variation d_vars[] =
{
  v0,
  v1,
  v2,
  v3,
  v4
};

FlameFun::FlameFun(const std::vector<double>& coef, const std::vector<Variation>& var)
  : _coef(coef)
  , _var(var)
  , _w(std::vector<double>(_var.size(), 1))
{ }

FlameFun::FlameFun(const std::vector<double>& coef,
                   const std::vector<Variation>& var,
                   const std::vector<double>& weight)
  : _coef(coef)
  , _var(var)
  , _w(weight)
{ }

__global__
void applyKernel(const WPoint *p, WPoint *np,
  double *coef,
  double *w,
  int lim,
  int *a)
{
  int idx = threadIdx.x + blockDim.x * blockIdx.x;
  if (idx < lim)
  {
    np->add(d_vars[3](coef[0] * p->x + coef[1] * p->y + coef[2],
                   coef[3] * p->x + coef[4] * p->y + coef[5]));
    np->mult(w[idx]);
  }
  *a += 12;
}

__global__
void simpleK(int *nb)
{
  *nb += 5;
}

WPoint FlameFun::apply(const WPoint& p)
{
  const int vsize = _var.size();
  const int csize = _coef.size();
  const int wsize = _w.size();

  WPoint *h_p, *d_p;
  h_p = new WPoint(p);
  cudaMalloc(&d_p, sizeof(WPoint));
  cudaMemcpy(d_p, h_p, sizeof(WPoint), cudaMemcpyHostToDevice);

  WPoint *h_np, *d_np;
  h_np = new WPoint();
  cudaMalloc(&d_np, sizeof(WPoint));
  cudaMemcpy(d_np, h_np, sizeof(WPoint), cudaMemcpyHostToDevice);

  double *h_coef, *d_coef;
  h_coef = _coef.data();
  cudaMalloc(&d_coef, csize * sizeof(double));
  cudaMemcpy(d_coef, h_coef, csize * sizeof(double), cudaMemcpyHostToDevice);

  double *h_w, *d_w;
  h_w = _w.data();
  cudaMalloc(&d_w, wsize * sizeof(double));
  cudaMemcpy(d_w, h_w, wsize * sizeof(double), cudaMemcpyHostToDevice);

  int *h_i, *d_i;
  h_i = new int(55);
  cudaMalloc(&d_i, sizeof(int));
  cudaMemcpy(d_i, h_i, sizeof(int), cudaMemcpyHostToDevice);
  //simpleK<<<1, 8>>>(d_i);

  applyKernel<<<1, 1>>>(d_p, d_np, d_coef, d_w, vsize, d_i);
  cudaMemcpy(h_np, d_np, sizeof(WPoint), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_i, d_i, sizeof(int), cudaMemcpyDeviceToHost);
  cudaDeviceSynchronize();
  //std::cout << *h_i << std::endl;
  cudaFree(d_np);
  cudaFree(d_p);
  cudaFree(d_coef);
  cudaFree(d_w);


  //cudaMemcpy(&h_np, d_np, sizeof(WPoint), cudaMemcpyDeviceToHost);
  /*for (int i = 0; i < lim; ++i)
  {
    h_np.add(_var[i](_coef[0] * p.x + _coef[1] * p.y + _coef[2],
                     _coef[3] * p.x + _coef[4] * p.y + _coef[5]));
    h_np.mult(_w[i]);
  }*/
  h_np->c = p.c;
  return *h_np;
}
