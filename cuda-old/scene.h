#pragma once

class Scene
{
public:
  Scene(int x_min, int x_max, int y_min, int y_max);
  void reset(int x_min, int x_max, int y_min, int y_max);

  int size() const;
  int height() const;
  int width() const;

  int x_min() const;
  int x_max() const;
  int y_min() const;
  int y_max() const;

private:
  int _x_min;
  int _x_max;
  int _y_min;
  int _y_max;
};
