#include <stdlib.h>
#include <cstdlib>
#include <iostream>

#include "flame.h"
#include "point.h"

Flame::Flame(const std::vector<FlameFun>& flameFun)
  : _flameFun(flameFun)
{ }

std::vector<unsigned int> Flame::run(unsigned int width, unsigned int height, unsigned int iter)
{
  srand(time(NULL));
  std::vector<unsigned int> histo(width * height, 0);

  double rnd1 = rand() > RAND_MAX ? -rand() : rand();
  double rnd2 = rand() > RAND_MAX ? -rand() : rand();
  WPoint p(rnd1 / RAND_MAX, rnd2 / RAND_MAX);

  const int lim = _flameFun.size();
  for (unsigned int i = 0; i < iter; ++i)
  {
    unsigned int j = rand();
    j %= lim;

    p = _flameFun[j].apply(p);
    int x = (p.x + 1) * width / 2;
    int y = (p.y + 1) * height / 2;

    histo[y * width + x] += 1;
  }

  return histo;
}
