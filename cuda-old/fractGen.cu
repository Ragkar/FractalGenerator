#include <png++/png.hpp>

#include "variation.h"
#include "flameFun.h"
#include "flame.h"

using Image = png::image<png::rgb_pixel>;

int main(int argc, char **argv)
{
  unsigned int max_iter = 1e7;
  unsigned int width = 1920;
  unsigned int height = 1080;

  std::vector<double> coef0 = {0.5, 0, 0, 0, 0.5, 0};
  std::vector<Variation> var0 = { v3 };
  FlameFun flameFun0(coef0, var0);

  std::vector<double> coef1 = {0.5, 0, 0.5, 0, 0.5, 0};
  std::vector<Variation> var1 = { v3 };
  FlameFun flameFun1(coef1, var1);

  std::vector<double> coef2 = {0.5, 0, 0, 0, 0.5, 0.5};
  std::vector<Variation> var2 = { v3 };
  FlameFun flameFun2(coef2, var2);

  Flame flame({flameFun0, flameFun1, flameFun2});
  auto histo = flame.run(width, height, max_iter);

  Image img(width, height);
  for (unsigned int x = 0; x < width; ++x)
  {
    for (unsigned int y = 0; y < height; ++y)
    {
      unsigned int i = histo[y * width + x];
      img[y][x] = png::rgb_pixel(i, i, i);
    }
  }
  img.write("fractal.png");

  return 0;
}
