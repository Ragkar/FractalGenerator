#pragma once

#include "flameFun.h"

class Flame
{
public:
  Flame(const std::vector<FlameFun>& flameFun);
  std::vector<unsigned int> run(unsigned int width, unsigned int height, unsigned int iter);

private:
  std::vector<FlameFun> _flameFun;
};
