#include <cmath>

#include "variation.h"

__device__ WPoint v0(double x, double y)
{
  return {x, y};
}

__device__ WPoint v1(double x, double y)
{
  return {std::sin(x), std::sin(y)};
}

__device__ WPoint v2(double x, double y)
{
  double r = std::sqrt(x * x + y * y);
  return { x / r, y / r};
}

__device__ WPoint v3(double x, double y)
{
  double r2 = x * x + y * y;
  return {x * std::sin(r2) - y * std::cos(r2),
          x * std::cos(r2) + y * std::sin(r2)};
}

__device__ WPoint v4(double x, double y)
{
  double r = std::sqrt(x * x + y * y);
  return {(x - y) * (x + y) / r, 2 * x * y / r};
}
