#include "color.h"

Color::Color()
  : r(0)
  , g(0)
  , b(0)
{ }

Color::Color(int c)
  : r(c < 0 ? 0 : c > 255 ? 255: c)
  , g(c < 0 ? 0 : c > 255 ? 255: c)
  , b(c < 0 ? 0 : c > 255 ? 255: c)
{ }

Color::Color(int red, int green, int blue)
  : r(red < 0 ? 0 : red > 255 ? 255 : red)
  , g(green < 0 ? 0 : green> 255 ? 255 : green)
  , b(blue < 0 ? 0 : blue > 255 ? 255 : blue)
{ }

png::rgb_pixel Color::to_pixel()
{
  return {r, g, b};
}

Color operator+(const Color& c1, const Color& c2)
{
  return {c1.r + c2.r, c1.g + c2.g, c1.b + c2.b};
}

Color operator-(const Color& c1, const Color& c2)
{
  return {c1.r - c2.r, c1.g - c2.g, c1.b - c2.b};
}

Color operator+(const Color& c, int coef)
{
  return {c.r + coef, c.g + coef, c.b + coef};
}

Color operator-(const Color& c, int coef)
{
  return {c.r - coef, c.g - coef, c.b - coef};
}

Color operator/(const Color& c, int coef)
{
  return {c.r / coef, c.g / coef, c.b / coef};
}

Color operator*(const Color& c, int coef)
{
  return {c.r * coef, c.g * coef, c.b * coef};
}
