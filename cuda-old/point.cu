#include "point.h"

__device__ __host__
WPoint::WPoint()
  : x(0)
  , y(0)
  , c(0)
{ }

__device__ __host__
WPoint::WPoint(const WPoint& p)
  : x(p.x)
  , y(p.y)
  , c(p.c)
{ }

__device__ __host__
WPoint::WPoint(double xx, double yy)
  : x(xx)
  , y(yy)
  , c(0)
{ }

__device__ __host__
WPoint::WPoint(double xx, double yy, unsigned int c)
  : x(xx)
  , y(yy)
  , c(c > 255 ? 255 : c)
{ }

__device__ __host__
void WPoint::add(const WPoint& p)
{
  this->x += p.x;
  this->y += p.y;
}

__device__ __host__
void WPoint::mult(double d)
{
  this->x *= d;
  this->y *= d;
}
