#pragma once

#include "point.h"
#include "variation.h"

#include <vector>


class FlameFun
{
public:
  FlameFun(const std::vector<double>& coef, const std::vector<Variation>& var);
  FlameFun(const std::vector<double>& coef, const std::vector<Variation>& var,
           const std::vector<double>& weight);

  WPoint apply(const WPoint& p);


private:
  std::vector<double> _coef;
  std::vector<Variation> _var;
  std::vector<double> _w;
};
