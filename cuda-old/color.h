#pragma once

#include <png++/png.hpp>

class Color
{
public:
  Color();
  Color(int c);
  Color(int r, int g, int b);

  png::rgb_pixel to_pixel();

  unsigned char r;
  unsigned char g;
  unsigned char b;
};

Color operator+(const Color& c1, const Color& c2);
Color operator/(const Color& c1, const Color& c2);

Color operator+(const Color& c1, int coef);
Color operator-(const Color& c1, int coef);
Color operator/(const Color& c1, int coef);
Color operator*(const Color& c1, int coef);
