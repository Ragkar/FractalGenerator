#pragma once

#include <functional>

#include "point.h"

using Variation = std::function<Point(double, double)>;

Point v0(double x, double y); // Linvear
Point v1(double x, double y); // Sinusoidal
Point v2(double x, double y); // Spherical
Point v3(double x, double y); // Swirl
Point v4(double x, double y); // Horseshoe
Point v5(double x, double y); // Polar
Point v6(double x, double y); // Handkerchief
Point v7(double x, double y); // Heart
Point v8(double x, double y); // Disc
Point v9(double x, double y); // Spiral
Point v10(double x, double y); // Hyperbolic
Point v11(double x, double y); // Diamond
Point v12(double x, double y); // Ex
Point v13(double x, double y); // Julia
