#pragma once

#include <png++/png.hpp>

class Color
{
public:
  Color();
  Color(double c);
  Color(double r, double g, double b);
  Color(double r, double g, double b, unsigned int alpha);

  void update_alpha();
  png::rgb_pixel to_pixel() const;

  void operator+=(const Color& c);
  void operator*=(const Color& c);

  friend Color operator+(const Color& c1, const Color& c2);
  friend Color operator-(const Color& c1, const Color& c2);

  friend Color operator+(const Color& c1, double coef);
  friend Color operator-(const Color& c1, double coef);
  friend Color operator/(const Color& c1, double coef);
  friend Color operator*(const Color& c1, double coef);

private:
  double _r;
  double _g;
  double _b;
  unsigned int _a;
};
