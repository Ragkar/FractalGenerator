#pragma once

class Point
{
public:
  Point();
  Point(const Point& p);
  Point(double x, double y);

  void operator+=(const Point& p);
  void operator*=(double d);

  double x;
  double y;
};
