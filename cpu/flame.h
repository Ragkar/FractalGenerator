#pragma once

#include "flameFun.h"

class Flame
{
public:
  Flame(const std::vector<FlameFun>& flameFun);
  Flame(const std::vector<FlameFun>& flameFun, const FlameFun& postTransform);

  std::vector<Color> run(const unsigned int& width,
                                const unsigned int& height,
                                const unsigned int& iter);

private:
  std::vector<FlameFun> _flameFun;
  FlameFun _postTransform;
};
