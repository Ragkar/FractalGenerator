#include <png++/png.hpp>

#include "variation.h"
#include "flameFun.h"
#include "flame.h"

using Image = png::image<png::rgb_pixel>;

Flame Sierpinski()
{
  const std::vector<Variation> var = { v1 };

  const std::vector<double> coef0 = { 0.5, 0, 0,   0, 0.5, 0   };
  const std::vector<double> coef1 = { 0.5, 0, 0.5, 0, 0.5, 0   };
  const std::vector<double> coef2 = { 0.5, 0, 0,   0, 0.5, 0.5 };

  FlameFun flameFun0(coef0, var);
  FlameFun flameFun1(coef1, var);
  FlameFun flameFun2(coef2, var);

  return Flame({ flameFun0, flameFun1, flameFun2 });
}

Flame flame1()
{
  const std::vector<Variation> var0 = { v2 };
  const std::vector<Variation> var1 = { v12 };
  const std::vector<Variation> var2 = { v6 };

  const std::vector<double> coef0 = { 0.56248,   0.397861, -0.539599,
                                      0.501088, -0.42992,  -0.112404};
  const std::vector<double> coef1 = { 0.76339,  -0.496174,  0.16248,
                                      0.320468,  0.87022,  -0.548389 };
  const std::vector<double> coef2 = { 0.946339, -0.496174,  0.15325,
                                      0.750468,  0.26342,   0.288389 };

  const Color c0 = Color(100, 89, 101) / 255;
  const Color c1 = Color(94, 103, 120) / 255;
  const Color c2 = Color(156, 108, 121) / 255;

  FlameFun flameFun0(coef0, var0, c0);
  FlameFun flameFun1(coef1, var1, c1);
  FlameFun flameFun2(coef2, var2, c2);

  return Flame({ flameFun0, flameFun1, flameFun2 });
}

Flame flame2()
{
  const std::vector<Variation> var0 = { v0 };
  const std::vector<Variation> var1 = { v9 };
  const std::vector<Variation> var2 = { v3 };

  const std::vector<double> coef0 = {  0.98396,   0.298328,  0.359416,
                                      -0.583541, -0.85059,  -0.378754};
  const std::vector<double> coef1 = { -0.900388,  0.293063,  0.397598,
                                       0.0225629, 0.465126, -0.277212};
  const std::vector<double> coef2 = { -0.329863, -0.0855261, -0.369381,
                                      -0.858379,  0.977861,   0.547595};

  const Color c0 = Color(226, 162, 29) / 255;
  const Color c1 = Color(222, 95, 76) / 255;
  const Color c2 = Color(198, 47, 15) / 255;

  FlameFun flameFun0(coef0, var0, c0);
  FlameFun flameFun1(coef1, var1, c1);
  FlameFun flameFun2(coef2, var2, c2);

  return Flame({ flameFun0, flameFun1, flameFun2 });
}

Flame flame3()
{
  const std::vector<Variation> var0 = { v11 };
  const std::vector<Variation> var1 = { v9 };
  const std::vector<Variation> var2 = { v7 };
  const std::vector<Variation> var3 = { v6 };
  const std::vector<Variation> var4 = { v8 };
  const std::vector<Variation> var5 = { v10 };
  const std::vector<Variation> var6 = { v12 };

  const std::vector<double> coef0 = { 0.25, 0, 0, 0.55, 1, 0 };
  const std::vector<double> coef1 = { 0.6, -0.25, 0.25, -0.4, 0.45, 0.55 };
  const std::vector<double> coef2 = { -0.25, 0, 0.25, -0.55, -0.55, 0.55 };
  const std::vector<double> coef3 = { 0.15, 0, 0.25, 0.2, 0.55, 0 };
  const std::vector<double> coef4 = { -0.6, -0.75, 1, 0.2, 0, 0 };
  const std::vector<double> coef5 = { -0.15, 0.45, 0.4, 0.35, -0.05, 0.2 };
  const std::vector<double> coef6 = { -0.45, 0.15, 0.85, 0.05, -0.15, 0.15 };

  const Color c0 = Color(212, 221, 62) / 255;
  const Color c1 = Color(163, 185, 53) / 255;
  const Color c2 = Color(190, 180, 128) / 255;
  const Color c3 = Color(210, 157, 127) / 255;
  const Color c4 = Color(176, 65, 48) / 255;
  const Color c5 = Color(251, 239, 160) / 255;
  const Color c6 = Color(255, 213, 160) / 255;

  FlameFun flameFun0(coef0, var0, c0);
  FlameFun flameFun1(coef1, var1, c1);
  FlameFun flameFun2(coef2, var2, c2);
  FlameFun flameFun3(coef3, var3, c3);
  FlameFun flameFun4(coef4, var4, c4);
  FlameFun flameFun5(coef5, var5, c5);
  FlameFun flameFun6(coef6, var6, c6);

  return Flame({ flameFun0, flameFun1, flameFun2, flameFun3, flameFun4,
                 flameFun5, flameFun6 });
}

int main(int argc, char **argv)
{
  unsigned int max_iter = 1e9;
  unsigned int width = 1920;
  unsigned int height = 1080;

  Flame flame = flame3();
  auto histo = flame.run(width, height, max_iter);

  Image img(width, height);
  for (unsigned int x = 0; x < width; ++x)
  {
    for (unsigned int y = 0; y < height; ++y)
    {
      img[y][x] = histo[y * width + x].to_pixel();
    }
  }
  img.write("fractal.png");

  return 0;
}
