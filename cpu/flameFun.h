#pragma once

#include <vector>

#include "color.h"
#include "point.h"
#include "variation.h"

class FlameFun
{
public:
  FlameFun();
  FlameFun(const std::vector<Variation>& var);
  FlameFun(const std::vector<double>& coef, const std::vector<Variation>& var);
  FlameFun(const std::vector<double>& coef, const std::vector<Variation>& var,
           const Color& color);
  FlameFun(const std::vector<double>& coef, const std::vector<Variation>& var,
           const std::vector<double>& weight);

  Point apply(const Point& p) const;
  Color getColor() const;

private:
  const std::vector<double> _coef;
  const std::vector<Variation> _var;
  const std::vector<double> _w;
  const Color _color;
};
