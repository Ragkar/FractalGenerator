#include <stdlib.h>
#include <cstdlib>
#include <iostream>

#include "flame.h"
#include "point.h"

Flame::Flame(const std::vector<FlameFun>& flameFun)
  : _flameFun(flameFun)
  , _postTransform()
{ }

Flame::Flame(const std::vector<FlameFun>& flameFun, const FlameFun& postTransform)
  : _flameFun(flameFun)
  , _postTransform(postTransform)
{ }

std::vector<Color> Flame::run(const unsigned int& width,
                                     const unsigned int& height,
                                     const unsigned int& iter)
{
  srand(time(NULL));
  std::vector<Color> histo(width * height);
  const unsigned int bnd = width * height;

  double rnd1 = rand() > RAND_MAX ? -rand() : rand();
  double rnd2 = rand() > RAND_MAX ? -rand() : rand();
  Point p(rnd1 / RAND_MAX, rnd2 / RAND_MAX);

  const int lim = _flameFun.size();
  for (unsigned int i = 0; i < iter; ++i)
  {
    unsigned int j = rand();
    j %= lim;

    p = _flameFun[j].apply(p);
    p = _postTransform.apply(p);
    const int x = (p.x + 1) * width / 2;
    const int y = (p.y + 1) * height / 2;

    const unsigned int idx = y * width + x;
    if (idx < bnd)
    {
      histo[idx] += _flameFun[j].getColor();
      histo[idx].update_alpha();
    }
    else
      --i;
  }

  return histo;
}
