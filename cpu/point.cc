#include "point.h"

Point::Point()
  : x(0)
  , y(0)
{ }

Point::Point(const Point& p)
  : x(p.x)
  , y(p.y)
{ }

Point::Point(double xx, double yy)
  : x(xx)
  , y(yy)
{ }

void Point::operator+=(const Point& p)
{
  this->x += p.x;
  this->y += p.y;
}

void Point::operator*=(double d)
{
  this->x *= d;
  this->y *= d;
}
