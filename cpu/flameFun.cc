#include "flameFun.h"

FlameFun::FlameFun()
  : _coef({ 1, 0, 0, 0, 1, 0 })
  , _var({ v0 })
  , _w(std::vector<double>(_var.size(), 1))
{ }

FlameFun::FlameFun(const std::vector<Variation>& var)
  : _coef({ 1, 0, 0, 0, 1, 0 })
  , _var(var)
  , _w(std::vector<double>(_var.size(), 1))
{ }

FlameFun::FlameFun(const std::vector<double>& coef, const std::vector<Variation>& var)
  : _coef(coef)
  , _var(var)
  , _w(std::vector<double>(_var.size(), 1))
{ }

FlameFun::FlameFun(const std::vector<double>& coef, const std::vector<Variation>& var,
                   const Color& color)
  : _coef(coef)
  , _var(var)
  , _w(std::vector<double>(_var.size(), 1))
  , _color(color)
{ }

FlameFun::FlameFun(const std::vector<double>& coef,
                   const std::vector<Variation>& var,
                   const std::vector<double>& weight)
  : _coef(coef)
  , _var(var)
  , _w(weight)
{ }

Point FlameFun::apply(const Point& p) const
{
  Point np;
  const int lim = _var.size();
  for (int i = 0; i < lim; ++i)
  {
    np += _var[i](_coef[0] * p.x + _coef[1] * p.y + _coef[2],
                  _coef[3] * p.x + _coef[4] * p.y + _coef[5]);
    np *= _w[i];
  }

  return np;
}

Color FlameFun::getColor() const
{
  return _color;
}
