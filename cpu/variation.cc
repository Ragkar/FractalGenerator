#define _USE_MATH_DEFINES

#include <cmath>

#include "variation.h"

Point v0(double x, double y)
{
  return { x, y };
}

Point v1(double x, double y)
{
  return { std::sin(x), std::sin(y) };
}

Point v2(double x, double y)
{
  // TODO: check if sqrt is needed
  double r = std::sqrt(x * x + y * y);
  return { x / r, y / r};
}

// TODO: check the other version of the swirl
Point v3(double x, double y)
{
  double r2 = x * x + y * y;
  return { x * std::sin(r2) - y * std::cos(r2),
           x * std::cos(r2) + y * std::sin(r2) };
}

Point v4(double x, double y)
{
  double r = std::sqrt(x * x + y * y);
  return { (x - y) * (x + y) / r, 2 * x * y / r };
}

Point v5(double x, double y)
{
  double theta = std::atan(x/y);
  double r = std::sqrt(x * x + y * y);
  return { theta / M_PI, r - 1 };
}

Point v6(double x, double y)
{
  double theta = std::atan(x/y);
  double r = std::sqrt(x * x + y * y);
  return { r * std::sin(theta + r) , r * std::cos(theta - r) };
}

Point v7(double x, double y)
{
  double theta = std::atan(x/y);
  double r = std::sqrt(x * x + y * y);
  return { r * std::sin(theta * r) , - r * std::cos(theta * r) };
}

Point v8(double x, double y)
{
  double theta = std::atan(x/y);
  double r = std::sqrt(x * x + y * y);
  return { (theta / M_PI) * std::sin(M_PI * r),
           (theta / M_PI) * std::cos(M_PI * r) };
}

Point v9(double x, double y)
{
  double theta = std::atan(x/y);
  double r = std::sqrt(x * x + y * y);
  return { (std::cos(theta) + std::sin(r)) / r,
           (std::sin(theta) - std::cos(r)) / r };
}

Point v10(double x, double y)
{
  double theta = std::atan(x/y);
  double r = std::sqrt(x * x + y * y);
  return { std::sin(theta) / r, r * std::cos(theta) };
}

Point v11(double x, double y)
{
  double theta = std::atan(x/y);
  double r = std::sqrt(x * x + y * y);
  return { (std::sin(theta) * std::cos(r)),
           (std::cos(theta) - std::sin(r)) };
}

Point v12(double x, double y)
{
  double r = std::sqrt(x * x + y * y);
  double theta = std::atan(x/y);
  double p0 = std::sin(theta + r);
  double p1 = std::cos(theta - r);
  p0 *= p0 * p0;
  p1 *= p1 * p1;
  return { r * (p0 + p1), r * (p0 - p1) };
}

Point v13(double x, double y)
{
  double r = std::sqrt(std::sqrt(x * x + y * y));
  double theta = std::atan(x/y) / 2;
  double omega = (rand() % 2) * M_PI;
  return { r * std::cos(theta + omega), r * std::sin(theta + omega) };
}
