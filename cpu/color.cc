#include "color.h"

#include <cmath>

Color::Color()
  : _r(0)
  , _g(0)
  , _b(0)
  , _a(1)
{ }

Color::Color(double c)
  : _r(c)
  , _g(c)
  , _b(c)
  , _a(1)
{ }

Color::Color(double red, double green, double blue)
  : _r(red)
  , _g(green)
  , _b(blue)
  , _a(1)
{ }

Color::Color(double red, double green, double blue, unsigned int alpha)
  : _r(red)
  , _g(green)
  , _b(blue)
  , _a(alpha)
{ }

void Color::update_alpha()
{
  if (_a + 1 > _a)
    ++_a;
}

png::rgb_pixel Color::to_pixel() const
{
  double s = (1 - (std::log1p(_a) / _a)) * 255.0;
  return png::rgb_pixel(_r / _a * s, _g / _a * s, _b / _a * s);
}

void Color::operator+=(const Color& c)
{
  _r += c._r;
  _g += c._g;
  _b += c._b;
}

void Color::operator*=(const Color& c)
{
  _r *= c._r;
  _g *= c._g;
  _b *= c._b;
}

Color operator+(const Color& c1, const Color& c2)
{
  return {c1._r + c2._r, c1._g + c2._g, c1._b + c2._b, c1._a};
}

Color operator-(const Color& c1, const Color& c2)
{
  return {c1._r - c2._r, c1._g - c2._g, c1._b - c2._b, c1._a};
}

Color operator+(const Color& c, double coef)
{
  return {c._r + coef, c._g + coef, c._b + coef, c._a};
}

Color operator-(const Color& c, double coef)
{
  return {c._r - coef, c._g - coef, c._b - coef, c._a};
}

Color operator/(const Color& c, double coef)
{
  return {c._r / coef, c._g / coef, c._b / coef, c._a};
}

Color operator*(const Color& c, double coef)
{
  return {c._r * coef, c._g * coef, c._b * coef, c._a};
}
